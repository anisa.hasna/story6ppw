from django.test import LiveServerTestCase, TestCase, Client
from django.urls import resolve
from .views import index

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

# Create your tests here.
class unitTest(TestCase):
	def test_apakah_ada_slash_about(self):
		c = Client()
		response = c.get("/about/")
		self.assertEqual(response.status_code, 200)

	def test_apakah_pake_template_abouthtml(self):
		response = Client().get('/about/')
		self.assertTemplateUsed(response, 'about.html')

	def test_apakah_pake_fungsi_index(self):
		found = resolve("/about/")
		self.assertEqual(found.func, index)

	def test_apakah_ada_button_change_theme(self):
		c = Client()
		response = c.get("/about/")
		content = response.content.decode('utf8')
		self.assertIn("<button", content)
		self.assertIn("Change Theme", content)

	def test_apakah_ada_a_aktivitas(self):
		c = Client()
		response = c.get("/about/")
		content = response.content.decode('utf8')
		self.assertIn("<a", content)
		self.assertIn("Aktivitas", content)

	def test_apakah_ada_a_organisasi(self):
		c = Client()
		response = c.get("/about/")
		content = response.content.decode('utf8')
		self.assertIn("<a", content)
		self.assertIn("Organsasi/Kepanitiaan", content)

	def test_apakah_ada_a_prestasi(self):
		c = Client()
		response = c.get("/about/")
		content = response.content.decode('utf8')
		self.assertIn("<a", content)
		self.assertIn("Prestasi", content)

class functionalTest(LiveServerTestCase):
	def setUp(self):
		chrome_options = Options()
		chrome_options.add_argument('--dns-prefetch-disable')
		chrome_options.add_argument('--no-sandbox')        
		chrome_options.add_argument('--headless')
		chrome_options.add_argument('disable-gpu')
		self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
		super(functionalTest, self).setUp()

	def tearDown(self):
		self.selenium.quit()
		super(functionalTest, self).tearDown()

	def test_apakah_web_terbuka_dan_accordion_button_bisa_dijalankan(self):
		selenium = self.selenium
		selenium.get('http://localhost:8000/about/')
		tmp_tema = selenium.find_element_by_id('gantitema')
		tmp_aktivitas = selenium.find_element_by_id('ui-id-1')
		tmp_organisasi = selenium.find_element_by_id('ui-id-3')
		tmp_prestasi = selenium.find_element_by_id('ui-id-5')

		tmp_tema.send_keys(Keys.RETURN)
		tmp_aktivitas.send_keys(Keys.RETURN)
		tmp_organisasi.send_keys(Keys.RETURN)
		tmp_prestasi.send_keys(Keys.RETURN)