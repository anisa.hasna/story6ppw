from django.test import LiveServerTestCase, TestCase, Client
from django.urls import resolve
from .models import statusModel
from .forms import statusForm
from .views import index

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time
from datetime import datetime

# Create your tests here.

class unitTest(TestCase):
	def test_apakah_ada_landing_page(self):
		c = Client()
		response = c.get("/")
		self.assertEqual(response.status_code, 200)

	def test_apakah_pake_template_landingpagehtml(self):
		response = Client().get('/')
		self.assertTemplateUsed(response, 'landingpage.html')

	def test_apakah_pake_fungsi_index(self):
		found = resolve("/")
		self.assertEqual(found.func, index)

	def test_apakah_ada_tulisan_halo(self):
		c = Client()
		response = c.get("/")
		content = response.content.decode('utf8')
		self.assertIn("HALO", content)

	def test_apakah_ada_tulisan_apa_kabar(self):
		c = Client()
		response = c.get("/")
		content = response.content.decode('utf8')
		self.assertIn("APA KABAR?", content)

	def test_apakah_ada_form(self):
		c = Client()
		response = c.get("/")
		content = response.content.decode('utf8')
		self.assertIn("<form", content)

	def test_apakah_ada_button_add(self):
		c = Client()
		response = c.get("/")
		content = response.content.decode('utf8')
		self.assertIn("<button", content)
		self.assertIn("Add", content)

	def test_apakah_ada_button_change_theme(self):
		c = Client()
		response = c.get("/")
		content = response.content.decode('utf8')
		self.assertIn("<button", content)
		self.assertIn("Change Theme", content)

	def test_model_statusModel(self):
		return statusModel.objects.create(status="alhamdulillah baik", waktu="2018-12-10 12:00")

	def test_form_statusForm(self):
		f = statusModel.objects.create(status="alhamdulillah baik", waktu="2018-12-10 12:00")
		data = {'status': f.status, 'waktu': f.waktu}
		form = statusForm(data=data)
		self.assertTrue(form.is_valid())

class functionalTest(LiveServerTestCase):
	def setUp(self):
		chrome_options = Options()
		chrome_options.add_argument('--dns-prefetch-disable')
		chrome_options.add_argument('--no-sandbox')        
		chrome_options.add_argument('--headless')
		chrome_options.add_argument('disable-gpu')
		self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
		super(functionalTest, self).setUp()

	def tearDown(self):
		self.selenium.quit()
		super(functionalTest, self).tearDown()

	def test_apakah_web_terbuka_dan_input_dapat_dimasukkan_serta_ditampilkan(self):
		selenium = self.selenium
		selenium.get(self.live_server_url)
		tmp_input = selenium.find_element_by_id('id_status')
		tmp_submit = selenium.find_element_by_id('submit')

		tmp_input.send_keys('Coba Coba')
		tmp_submit.send_keys(Keys.RETURN)

		self.assertIn('Coba Coba', selenium.page_source)
