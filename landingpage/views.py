from django.shortcuts import render, redirect

from .forms import statusForm
from .models import statusModel
# Create your views here.

def index(request):
	statusform = statusForm(request.POST or None)

	if request.method == 'POST':
		if statusform.is_valid():
			statusform.save()

			return redirect('/')


	statusmodel = statusModel.objects.all()

	context = {
		'data_form' : statusform,
		'data_model' : statusmodel,
	}

	return render(request,'landingpage.html', context)
