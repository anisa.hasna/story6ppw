from django import forms
from django.forms import TextInput
from .models import statusModel

class statusForm(forms.ModelForm):
	class Meta:
		model = statusModel
		fields =[
			'status',
		]
		widgets ={
		'status': TextInput(attrs={'placeholder': 'max 300 characters'}),
		}