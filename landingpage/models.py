from django.db import models
from django.utils import timezone

# Create your models here.

class statusModel(models.Model):
    
    status = models.CharField(max_length=300)
    waktu = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return "{}".format(self.status)
